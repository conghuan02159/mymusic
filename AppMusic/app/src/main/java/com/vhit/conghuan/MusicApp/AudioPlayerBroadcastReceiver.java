package com.vhit.conghuan.MusicApp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AudioPlayerBroadcastReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {

		String action = intent.getAction();

		if (action.equalsIgnoreCase("play")) {
			if (Playmusic.mp.isPlaying() == true) {
				Playmusic.btnPlay.setImageResource(R.drawable.btn_play);
				Playmusic.mp.pause();
				Playmusic.nPanel.remoteView.setImageViewResource(R.id.btns,
						R.drawable.apollo_holo_dark_play);
				Playmusic.nPanel.remoteView2.setImageViewResource(R.id.btns,
						R.drawable.apollo_holo_dark_play);
				Playmusic.nPanel.build();
			} else {
				Playmusic.btnPlay.setImageResource(R.drawable.btn_pause);

				Playmusic.mp.start();
				Playmusic.nPanel.remoteView.setImageViewResource(R.id.btns,
						R.drawable.apllo_media_pause);
				Playmusic.nPanel.remoteView2.setImageViewResource(R.id.btns,
						R.drawable.apllo_media_pause);
				Playmusic.nPanel.build();
			}
		} else if (action.equalsIgnoreCase("next")) {
			Playmusic.btnNext.performClick();
			Playmusic.nPanel.remoteView.setImageViewResource(R.id.btns,
					R.drawable.apllo_media_pause);
			Playmusic.nPanel.remoteView2.setImageViewResource(R.id.btns,
					R.drawable.apllo_media_pause);
			Playmusic.nPanel.build();


		} else if (action.equalsIgnoreCase("back")) {
			Playmusic.btnPrevious.performClick();
			Playmusic.nPanel.remoteView.setImageViewResource(R.id.btns,
					R.drawable.apllo_media_pause);
			Playmusic.nPanel.remoteView2.setImageViewResource(R.id.btns,
					R.drawable.apllo_media_pause);
			Playmusic.nPanel.build();

		}
//		else{
//			action.compareToIgnoreCase("out"){
//
//			}
//
//		}
	}
}