package com.vhit.conghuan.MusicApp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class Danhsach extends Activity {
    ProgressDialog pausingDialog;
    AlertDialog.Builder alertDialogBuilder;
    SimpleAdapter adapter;
    Context a;
    EditText search;
    ListView lv;
    Thread ts;
    private  MediaPlayer mp;
    AlertDialog.Builder b;
    ArrayList<HashMap<String, Object>> arrayTemplist;
    static   ArrayList<HashMap<String, Object>> songsList = new ArrayList<HashMap<String, Object>>();
    static ArrayList<HashMap<String, Object>> songsListData = new ArrayList<HashMap<String, Object>>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.danhsach_activity);
        a = this;

        search = (EditText) findViewById(R.id.search);
        lv = (ListView) findViewById(R.id.lisviewss);
        pausingDialog = new ProgressDialog(this);
//
        search = (EditText) findViewById(R.id.search);
        getWindow().setBackgroundDrawableResource(R.drawable.nen);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        adapter = new SimpleAdapter(this, songsListData, R.layout.list_row,
                new String[]{"anh", "songTitle", "songTotal", "songCasi",
                        "song"}, new int[]{R.id.list_image, R.id.title,
                R.id.duration, R.id.artist, R.id.chatluong}) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);
                return view;
            }
        };
        adapter.setViewBinder(new SimpleAdapter.ViewBinder() {
            @Override
            public boolean setViewValue(View view, Object data,
                                        String textRepresentation) {
                if (view.getId() == R.id.list_image) {
                    ImageView imageView = (ImageView) view;
                    Bitmap drawable = (Bitmap) data;
                    imageView.setImageBitmap(drawable);
                    return true;
                }
                return false;
            }
        });
//        alertDialogBuilder = new AlertDialog.Builder(getActivity());
        if (songsList.size() == 0) {

            showChangeLangDialog();

        } else {
            this.songsList = Playmusic.songsList;
            lv.setAdapter(adapter);
        }

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                int songIndex = position;
                Intent in = new Intent(Danhsach.this,
                        Playmusic.class);
                in.putExtra("songIndex", songIndex);
                setResult(100, in);
                finish();
            }
        });
    }
//        search.addTextChangedListener(new TextWatcher() {
//
//            @Override
//            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
//                // When user changed the Text
//                Danhsach.this.songsListData.getFilter().filter(cs);
//            }
//
//            @Override
//            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
//                                          int arg3) {
//                // TODO Auto-generated method stub
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable arg0) {
//                // TODO Auto-generated method stub
//            }
//        });
//    }
    public void showChangeLangDialog() {
        b = new AlertDialog.Builder(this);
        b.setTitle("Thông báo!");
        b.setMessage("Yêu cầu thư mục cập nhật bài hát là ../Music !");
        b.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public ArrayList<HashMap<String, Object>> songsList;
            @Override
            public void onClick(DialogInterface dialog, int which) {
                b.setMessage("Cập nhật danh sách?");
                this.songsList = getPlayList();
                songsListData.clear();
//                if (Playmusic.load == 0)
//                {
//                    Playmusic.load = 1;

                        MyTask myTask = new MyTask(Danhsach.this);
                        myTask.execute("parameter");

//                } else {
//                    this.songsList = Playmusic.songsList;
//                    for (int i = 0; i < songsList.size(); i++) {
//                        HashMap<String, Object> song = songsList.get(i);
//                        songsListData.add(song);
//                    }
//                    lv.setAdapter(adapter);
//                }
            }


        });
        b.setNegativeButton("No", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        b.create().show();
    }
    //======================================================================================
    //doc danh sach
    //========================================================================================

    final static String MEDIA_PATH = "/sdcard/Music"; //bo nho trong
    final static String MEDIA_PATH2 = "/sdcard0/Music";
    String total, bitrate, casi, date, title;

    @SuppressLint("NewApi")
    protected static ArrayList<HashMap<String, Object>> getPlayList() {
        return songsList;
    }

    public ListView getListView() {
        return lv;
    }

    class MyTask extends AsyncTask<String, Integer, String> {
        Runnable changeMessage;
        ProgressDialog pausingDialog;
        public MyTask(Context context1) {
            a = context1;
            songsList.clear();
            songsListData.clear();
        }
        protected void onPreExecute() {
            pausingDialog = new ProgressDialog(a);
            pausingDialog.setCancelable(true);
            pausingDialog.setTitle("Đang cập nhật danh sách");
            pausingDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            pausingDialog.setIndeterminate(true);
            pausingDialog.setIndeterminateDrawable(getResources().getDrawable(
                    R.anim.progress_dialog_icon_drawable_animation));
            pausingDialog.setCancelable(false);
            pausingDialog.show();
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... params) {
            // TODO Auto-generated method stub
            System.out.println(MEDIA_PATH);
            if (MEDIA_PATH != null) {
                File home = new File(MEDIA_PATH);
                File[] listFiles = home.listFiles();
                if (listFiles != null && listFiles.length > 0) {
                    for (final File file : listFiles) {
                        if (file.isFile()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (file.getName().endsWith(".mp3")
                                            || file.getName().endsWith("MP3")) {
                                        pausingDialog
                                                .setMessage(file.getName());
                                    }
                                }
                            });
                            addSongToList(file);
                        }
                    }
                }
            }
            if (MEDIA_PATH2 != null) {
                File home = new File(MEDIA_PATH2);
                File[] listFiles = home.listFiles();
                if (listFiles != null && listFiles.length > 0) {
                    for (final File file : listFiles) {
                        if (file.isFile()) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (file.getName().endsWith(".mp3")
                                            || file.getName().endsWith("MP3")) {
                                        pausingDialog
                                                .setMessage(file.getName());
                                    }
                                }
                            });
                            addSongToList(file);
                        }
                    }
                }
            }
            Collections.sort(songsList,
                    new Comparator<HashMap<String, Object>>() {
                        @Override
                        public int compare(HashMap<String, Object> o1,
                                           HashMap<String, Object> o2) {
                            return ((String) o1.get("songTitle"))
                                    .compareToIgnoreCase((String) o2
                                            .get("songTitle"));
                        }
                    });
            for (int i = 0; i < songsList.size(); i++) {
                HashMap<String, Object> song = songsList.get(i);
                songsListData.add(song);
                Collections.sort(songsListData,
                        new Comparator<HashMap<String, Object>>() {
                            @Override
                            public int compare(HashMap<String, Object> o1,
                                               HashMap<String, Object> o2) {
                                return ((String) o1.get("songTitle"))
                                        .compareToIgnoreCase((String) o2
                                                .get("songTitle"));
                            }
                        });
            }
            Collections.sort(songsListData,
                    new Comparator<HashMap<String, Object>>() {
                        @Override
                        public int compare(HashMap<String, Object> o1,
                                           HashMap<String, Object> o2) {

                            return ((String) o1.get("songTitle"))
                                    .compareToIgnoreCase((String) o2
                                            .get("songTitle"));
                        }
                    });
            return "finish";

        }

        protected void onPostExecute(String result) {
            pausingDialog.dismiss();
            if (songsList.size() == 0) {
                Toast.makeText(Danhsach.this, "Không có bài hát trong thư mục ../Music hoặc thư mục không tồn tại!",
                        Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(Danhsach.this,
                        "Hiện có " + songsList.size() + " bài hát",
                        Toast.LENGTH_LONG).show();
            }
            lv.setAdapter(adapter);
        }

        private void addSongToList(File song) {

            if (song.getName().endsWith(".mp3") || song.getName().endsWith("MP3")) {
                MediaMetadataRetriever metaRetriever = new MediaMetadataRetriever();
                metaRetriever.setDataSource(song.getPath());
                Bitmap bitmap;
                byte[] album = metaRetriever.getEmbeddedPicture();
                if (album != null) {
                    bitmap = BitmapFactory.decodeByteArray(album, 0,
                            album.length);
                    bitmap = Bitmap.createScaledBitmap(bitmap, 110, 110, true);
                } else {
                    bitmap = BitmapFactory.decodeResource(getResources(),
                            R.drawable.bainhacmusic);
                    bitmap = Bitmap.createScaledBitmap(bitmap, 110, 110, true);
                }
                long bit;
                // get mp3 info
                // convert duration to minute:seconds
                String duration;
                duration = metaRetriever
                        .extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                title = metaRetriever
                        .extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
                casi = metaRetriever
                        .extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                if (title == null) {
                    title = song.getName().substring(0,
                            (song.getName().length() - 4));
                }
                if (casi == null) {
                    casi = "Không xác định";
                }
                if ((date) == null) {
                    date = "Không xác định";
                }
                long dur = Long.parseLong(duration);
                String seconds = String.valueOf((dur % 60000) / 1000);
                String minutes = String.valueOf(dur / 60000);
                if (seconds.length() == 1) {
                    total = minutes + ":0" + seconds;
                } else {
                    total = minutes + ":" + seconds;
                }
                long s = (dur % 60000) / 1000;
                long m = dur / 1000;
                bit = ((song.length() * 8 / (s + m)) / 1000);
                // close object
                metaRetriever.release();
                final HashMap<String, Object> songMap = new HashMap<String, Object>();
                songMap.put("songTitle",
                        title);
                bitrate = bit + " Kbps";
                songMap.put("anh", bitmap);
                songMap.put("songPath", song.getPath());
                songMap.put("songTotal", total);
                songMap.put("song", bitrate);
                songMap.put("songCasi", casi);
                // Adding each song to SongList
                songsList.add(songMap);
            }
        }
    }

    public String trim(String str) {
        str = str.replaceAll("\\s+", " ");
        str = str.replaceAll("(^\\s+|\\s+$)", "");
        return str;
    }

}