package com.vhit.conghuan.MusicApp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.widget.RemoteViews;

public class NotificationPanel {

	private static Context parent;
	 Notification foregroundNote;
	 NotificationManager nManager;
	 Notification.Builder nBuilder;
	 RemoteViews remoteView, remoteView2;
	 String dangphat;
	 PendingIntent btn12;
	public NotificationPanel(Context parent) {
		// TODO Auto-generated constructor stub
		NotificationPanel.parent = parent;
		remoteView = new RemoteViews(parent.getPackageName(), R.layout.main);
		remoteView2 = new RemoteViews(parent.getPackageName(), R.layout.main2);
		// set the button listeners
		setListeners(remoteView);
		setListeners(remoteView2);

		Intent volume1 = new Intent(parent, NotificationReturnSlot.class);
		PendingIntent btn12 = PendingIntent.getActivity(parent, 0, volume1, 0);
		nBuilder = new Notification.Builder(parent);

		foregroundNote = nBuilder.setSmallIcon(R.drawable.disc)

		.setOngoing(true).setTicker(Playmusic.tenbaihat)

		.setContentIntent(btn12).setContent(remoteView)
				.setWhen(System.currentTimeMillis())

				.setPriority(Notification.PRIORITY_MAX).build();

		foregroundNote.bigContentView = remoteView2;

		nManager = (NotificationManager) parent
				.getSystemService(Context.NOTIFICATION_SERVICE);
		nManager.notify(1, foregroundNote);
	}

	public  void setListeners(RemoteViews view) {
		// listener 1

		if (Playmusic.album != null) {

			if (Playmusic.tenbaihat.equals(dangphat) == false) {
				remoteView.setImageViewBitmap(R.id.album, Playmusic.album);
				remoteView2.setImageViewBitmap(R.id.album, Playmusic.album);
			}

		} else if (Playmusic.tenbaihat.equals(dangphat) == false) {
			remoteView.setImageViewResource(R.id.album, R.drawable.disc);
			remoteView2.setImageViewResource(R.id.album, R.drawable.disc);

		}
		view.setTextViewText(R.id.tenbaihat, Playmusic.tenbaihat);
		view.setTextViewText(R.id.casi, Playmusic.casi);

		Intent volume = new Intent("play");
		PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(parent,
				100, volume, 0);
		view.setOnClickPendingIntent(R.id.btns, pendingSwitchIntent);

		Intent next = new Intent("next");
		PendingIntent pendingSwitchIntent1 = PendingIntent.getBroadcast(parent,
				100, next, 0);
		view.setOnClickPendingIntent(R.id.btnb, pendingSwitchIntent1);

		Intent back = new Intent("back");
		PendingIntent pendingSwitchIntent2 = PendingIntent.getBroadcast(parent,
				100, back, 0);
		view.setOnClickPendingIntent(R.id.btnp, pendingSwitchIntent2);
	}


	public  void build() {
		Intent volume1 = new Intent(parent, NotificationReturnSlot.class);
		PendingIntent btn12 = PendingIntent.getActivity(parent, 0, volume1, 0);
		setListeners(remoteView);
		setListeners(remoteView2);
		foregroundNote = nBuilder.setSmallIcon(R.drawable.disc)

		.setOngoing(true).setTicker(Playmusic.tenbaihat)

		.setContentIntent(btn12).setContent(remoteView)
				.setWhen(System.currentTimeMillis())
				.setPriority(Notification.PRIORITY_MAX).build();
		foregroundNote.bigContentView = remoteView2;
		nManager.notify(1, foregroundNote);
	}

	public void notificationCancel() {
		nManager.cancel(1);
	}

	public void setten(String ten) {
		nBuilder.setTicker(ten);

	}

}