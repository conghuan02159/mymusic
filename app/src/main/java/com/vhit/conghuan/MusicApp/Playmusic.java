package com.vhit.conghuan.MusicApp;


import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.vhit.conghuan.MusicApp.visualizer.VisualizerView;
import com.vhit.conghuan.MusicApp.visualizer.renderer.BarGraphRenderer;
import com.vhit.conghuan.MusicApp.visualizer.renderer.CircleBarRenderer;
import com.vhit.conghuan.MusicApp.visualizer.renderer.CircleRenderer;
import com.vhit.conghuan.MusicApp.visualizer.renderer.LineRenderer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Playmusic extends Fragment implements SeekBar.OnSeekBarChangeListener, MediaPlayer.OnCompletionListener {


    public static ArrayList<HashMap<String,Object>> songsList;
    int indexSelected;
    public boolean h1=false,h2=false,h3=false,h4=false,isChecked=false;
    private ImageView btnPlay;
    private ImageView btnNext;
    private ImageView btnPrevious;
    private ImageView btnPlaylist;
    private ImageView btnRepeat;
    private ImageView btnShuffle;
    private SeekBar songProgressBar;
    private TextView songTitleLabel;
    private TextView songCurrentDurationLabel;
    private TextView songTotalDurationLabel;
    static Bitmap album;
    Animation fadeIn, fadeOut;
    private Danhsach danhsach;
    private Utilities utils;
    static String casi;
    int thoatgame;
    AlertDialog dialog;
    static String tenbaihat;
    public static MediaPlayer mp;
    int currentPosition;
    VisualizerView mVisualizerView;
    private int currentSongIndex = 0;
    private boolean isShuffle = false;
    private boolean isRepeat = false;
    private Handler mHandler = new Handler();
    private ImageView hu,ca,volume;
    CheckBox c1,c2;
    private AudioManager audioManager = null;
    Boolean b1,b2=false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.playmusic_activity, container, false);
        c1 = (CheckBox) v.findViewById(R.id.s1);
        volume = (ImageView) v.findViewById(R.id.imvvolum);
        hu = (ImageView) v.findViewById(R.id.buttonhieuung);
        ca = (ImageView) v.findViewById(R.id.buttonchinham);
        btnPlay = (ImageView) v.findViewById(R.id.btnPlay);
        btnNext = (ImageView) v.findViewById(R.id.btnNext);
        btnPrevious = (ImageView) v.findViewById(R.id.btnPrevious);
        btnPlaylist = (ImageButton) v.findViewById(R.id.btnPlaylist);
        btnRepeat = (ImageView) v.findViewById(R.id.btnRepeat);
        btnShuffle = (ImageView) v.findViewById(R.id.btnShuffle);
        songProgressBar = (SeekBar) v.findViewById(R.id.songProgressBar);
        songTitleLabel = (TextView) v.findViewById(R.id.songTitle);
        songCurrentDurationLabel = (TextView) v.findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = (TextView) v.findViewById(R.id.songTotalDurationLabel);

        mp = new MediaPlayer();
        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if(  audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC)==0)
        {
            volume.setImageResource(R.drawable.soundoff);
        }
        else
        {
            volume.setImageResource(R.drawable.soundon);
        }
        if(mp.isPlaying()){

        }else{
            danhsach = new Danhsach();
            utils = new Utilities();
            songsList = Danhsach.getPlayList();

            mVisualizerView = (VisualizerView) v.findViewById(R.id.visualizerView);
            mVisualizerView.link(mp);
            hu.setEnabled(false);
            songProgressBar.setOnSeekBarChangeListener(this); // Important
            mp.setOnCompletionListener(this); // Important

            fadeIn = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_in);
            fadeOut = AnimationUtils.loadAnimation(getActivity(), R.anim.fade_out);

        }

                c1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        // TODO Auto-generated method stub
                        if(c1.isChecked()==true)
                        {
                            b1=true;
                            hu.setEnabled(true);
                            c1.setText("ON");
                            hu.setImageResource(R.drawable.hieuungtrang);
                        }else {
                            b1=false;
                            hu.setEnabled(false);
                              cleanUp();
                            hu.setImageResource(R.drawable.hieuung);
                            c1.setText("OFF");
                        }
                    }
                });
        if(ca.isClickable()==true){
            b2=true;
            ca.setImageResource(R.drawable.equalizera);
        }
        else{
            b2=false;
            ca.setImageResource(R.drawable.equalizerawhite);
        }
            ca.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), Chinham.class);

                    startActivityForResult(i, 50);

                }
            });

        volume.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                ShowDialog();
            }
        });
        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (mp.isPlaying()) {
                    if (mp != null) {
                        mp.pause();
                        btnPlay.setImageResource(R.drawable.btn_play);
                    }
                } else {
                    if (mp != null) {
                        mp.start();
                        // Changing button image to pause button
//                        nPanel.remoteView.setImageViewResource(R.id.btns,
//                                R.drawable.img_btn_pause2);
//                        nPanel.remoteView2.setImageViewResource(R.id.btns,
//                                R.drawable.img_btn_pause2);
//                        nPanel.build();
                        btnPlay.setImageResource(R.drawable.a1);
                    }
                }
            }
        });
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if (currentSongIndex < (songsList.size() - 1)) {
                    playSong(currentSongIndex + 1);
                    currentSongIndex = currentSongIndex + 1;
                } else {
                    playSong(0);
                    currentSongIndex = 0;
                }
            }
        });
        btnPlaylist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent i = new Intent(getActivity(), Danhsach.class);
                startActivityForResult(i, 100);
            }
        });
        btnRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(isRepeat){
                    isRepeat = false;
                    Toast.makeText(getActivity(), "Repeat is OFF", Toast.LENGTH_SHORT).show();
                    btnRepeat.setImageResource(R.drawable.btn_repeat);
                }else{
                    // make repeat to true
                    isRepeat = true;
                    Toast.makeText(getActivity(), "Repeat is ON", Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    isShuffle = false;
                    btnRepeat.setImageResource(R.drawable.btn_repeat_focused);
                    btnShuffle.setImageResource(R.drawable.btn_shuffle);
                }
            }
        });
        btnShuffle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(isShuffle){
                    isShuffle = false;
                    Toast.makeText(getActivity(), "Shuffle is OFF", Toast.LENGTH_SHORT).show();
                    btnShuffle.setImageResource(R.drawable.btn_shuffle);
                }else{
                    // make repeat to true
                    isShuffle= true;
                    Toast.makeText(getActivity(), "Shuffle is ON", Toast.LENGTH_SHORT).show();
                    // make shuffle to false
                    isRepeat = false;
                    btnShuffle.setImageResource(R.drawable.btn_shuffle_focused);
                    btnRepeat.setImageResource(R.drawable.btn_repeat);
                }
            }
        });
        hu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showdialoghu();

            }
        });
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                if(currentSongIndex > 0){
                    playSong(currentSongIndex - 1);
                    currentSongIndex = currentSongIndex - 1;
                }else{
                        playSong(songsList.size() - 1);
                    currentSongIndex = songsList.size() - 1;
                }

            }
        });
        return v;
    }
    private void addBarGraphRenderers()
    {
        Paint paint = new Paint();
        paint.setStrokeWidth(50f);
        paint.setAntiAlias(true);
        paint.setColor(Color.argb(200, 56, 138, 252));
        BarGraphRenderer barGraphRendererBottom = new BarGraphRenderer(16, paint, false);
        mVisualizerView.addRenderer(barGraphRendererBottom);

        Paint paint2 = new Paint();
        paint2.setStrokeWidth(12f);
        paint2.setAntiAlias(true);
        paint2.setColor(Color.argb(200, 181, 111, 233));
        BarGraphRenderer barGraphRendererTop = new BarGraphRenderer(4, paint2, true);
        mVisualizerView.addRenderer(barGraphRendererTop);
    }

    private void addCircleBarRenderer()
    {
        Paint paint = new Paint();
        paint.setStrokeWidth(8f);
        paint.setAntiAlias(true);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.LIGHTEN));
        paint.setColor(Color.argb(255, 222, 92, 143));
        CircleBarRenderer circleBarRenderer = new CircleBarRenderer(paint, 32, true);
        mVisualizerView.addRenderer(circleBarRenderer);
    }

    private void addCircleRenderer()
    {
        Paint paint = new Paint();
        paint.setStrokeWidth(3f);
        paint.setAntiAlias(true);
        paint.setColor(Color.argb(255, 222, 92, 143));
        CircleRenderer circleRenderer = new CircleRenderer(paint, true);

        mVisualizerView.addRenderer(circleRenderer);
    }

    private void addLineRenderer()
    {
        Paint linePaint = new Paint();
        linePaint.setStrokeWidth(1f);
        linePaint.setAntiAlias(true);
        linePaint.setColor(Color.argb(0, 0, 0, 0));

        Paint lineFlashPaint = new Paint();
        lineFlashPaint.setStrokeWidth(5f);
        lineFlashPaint.setAntiAlias(true);
        lineFlashPaint.setColor(Color.argb(188, 255, 255, 255));
        LineRenderer lineRenderer = new LineRenderer(linePaint, lineFlashPaint, true);
        mVisualizerView.addRenderer(lineRenderer);
    }

    @Override
    public void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 100) {
            currentSongIndex = data.getExtras().getInt("songIndex");
            playSong(currentSongIndex);
        }

    }




    public void playSong(int songIndex) {
        // Play song
        try {
            if (songsList.isEmpty()) {
                khongnhac();
                return;
            }
            mp.reset();
            mp.setDataSource((String) songsList.get(songIndex).get("songPath"));
            mp.prepare();
            mp.start();
            startPressed();

            String songTitle = (songsList.get(songIndex).get("songTitle")
                    + " - " + (songsList.get(songIndex).get("songCasi")));
            songTitleLabel.setText(songTitle);
            tenbaihat = songTitle;
            casi = (String) songsList.get(songIndex).get("songCasi");
            album = (Bitmap) songsList.get(songIndex).get("anh");
            // Changing Button Image to pause image
            btnPlay.setImageResource(R.drawable.a1);
            // set Progress bar values
            songProgressBar.setProgress(0);
            songProgressBar.setMax(100);
            System.gc();
            // Updating progress bar
            updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void showdialoghu(){
        final CharSequence[] items = {" BAR "," CIRCLE "," CIRCLE BAR "," LINE "};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("add effects!");
        builder.setMultiChoiceItems(items, null,
                new DialogInterface.OnMultiChoiceClickListener() {
                    // indexSelected contains the index of item (of which checkbox checked)
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected,
                                        boolean isChecked) {
                        if (isChecked==true) {
                            if (indexSelected == 0) {
                                h1 = true;
                                addBarGraphRenderers();
                            }
                            if (indexSelected == 1) {
                                Toast.makeText(getActivity(), "" + indexSelected, Toast.LENGTH_LONG).show();
                                h2 = true;
                                addCircleRenderer();
                            }
                            if (indexSelected == 2) {
                                h3 = true;
                                addCircleBarRenderer();
                            }
                            if (indexSelected == 3) {
                                h4 = true;
                                addLineRenderer();
                            }
                        }
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (isChecked == true) {
                            Toast.makeText(getActivity(), "  da them hieu ung", Toast.LENGTH_LONG).show();
                        }
                        Toast.makeText(getActivity(), "" + indexSelected, Toast.LENGTH_LONG).show();


                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                    }
                });
        dialog = builder.create();//AlertDialog dialog; create like this outside onClick
        dialog.show();
    }
    private void cleanUp()
    {
        mVisualizerView.clearRenderers();
    }
    public void startPressed() throws IllegalStateException, IOException
    {
        if(mp.isPlaying())
        {
            return;
        }
        mp.prepare();
        mp.start();
    }
    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        @Override
        public void run() {
            if (thoatgame == 1)
            {
                getActivity().finish();
            } else {
                long totalDuration = mp.getDuration();
                long currentDuration = mp.getCurrentPosition();
                songTotalDurationLabel.setText(""
                        + utils.milliSecondsToTimer(totalDuration));
                songCurrentDurationLabel.setText(""
                        + utils.milliSecondsToTimer(currentDuration));
                // Updating progress bar
                long progress = (long) (utils.getProgressPercentage(
                        currentDuration, totalDuration));
                Log.d("Progress", "" + progress);
                songProgressBar.setProgress((int) progress);
//                 Running this thread after 100 milliseconds
                mHandler.postDelayed(this, 100);
            }
        }
    };
    @TargetApi(Build.VERSION_CODES.M)
    public void ShowDialog(){
        final AlertDialog.Builder popDialog = new AlertDialog.Builder(getActivity());
        final LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View Viewlayout = inflater.inflate(R.layout.volume_dialog,(ViewGroup) getActivity().findViewById(R.id.layout_dialog));
        final TextView item1 = (TextView)Viewlayout.findViewById(R.id.txtItem1);
        popDialog.setIcon(android.R.drawable.btn_star_big_on);
        popDialog.setTitle("Điều chỉnh âm lượng");
        popDialog.setView(Viewlayout);
        SeekBar volumeSeekbar = (SeekBar) Viewlayout.findViewById(R.id.seekBar1);

        volumeSeekbar.setMax(audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        volumeSeekbar.setProgress(audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));
        item1.setText("Âm lượng : " + (audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC)));
        volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onStopTrackingTouch(SeekBar arg0)
            {
            }
            @Override
            public void onStartTrackingTouch(SeekBar arg0)
            {
            }
            @Override
            public void onProgressChanged(SeekBar arg0, int progress, boolean arg2)
            {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        progress, 0);
                item1.setText("Âm lượng : " + progress);
                if(progress==0)
                {
                    volume.setImageResource(R.drawable.soundoff);
                }
                else
                {
                    volume.setImageResource(R.drawable.soundon);
                }
            }
        });
        popDialog.setPositiveButton("OK",
                new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }



                });
        popDialog.create();
        popDialog.show();
    }
    public void khongnhac() {
        Toast.makeText(getActivity(),
                "Không có nhạc trong thẻ nhớ và bộ nhớ trong", Toast.LENGTH_LONG).show();
        songTitleLabel.setText("Không có nhạc.Vui lòng thêm nhạc");
    }
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = mp.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(),
                totalDuration);
        // forward or backward to certain seconds
        mp.seekTo(currentPosition);
        this.currentPosition = currentPosition;
        // update timer progress again
        updateProgressBar();
    }
    @Override
    public void onCompletion(MediaPlayer mp) {
       // check for repeat is ON or OFF
        if (isRepeat) {
            // repeat is on play same song again
            playSong(currentSongIndex);
        } else if (isShuffle) {
            // shuffle is on - play a random song
            Random rand = new Random();
            currentSongIndex = rand.nextInt((songsList.size() - 1) - 0 + 1) + 0;
            playSong(currentSongIndex);
        } else {
            // no repeat or shuffle ON - play next song
            if (currentSongIndex < (songsList.size() - 1)) {
                playSong(currentSongIndex + 1);
                currentSongIndex = currentSongIndex + 1;
            } else {
                // play first song
                playSong(0);
                currentSongIndex = 0;
            }
        }
    }



}

